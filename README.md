# RMRR Removed PVE Kernel
This project is used to build a Proxmox VE kernel which removed RMRR check.

This kernel may help if you get the following log when trying to enable IOMMU on an Intel CPU.

```
Device is ineligible for IOMMU domain attach due to platform RMRR requirement.  Contact your platform vendor.
```

**RELATED ISSUES**:
- https://forum.proxmox.com/threads/compile-proxmox-ve-with-patched-intel-iommu-driver-to-remove-rmrr-check.36374/
- https://forum.proxmox.com/threads/help-with-pci-passthrough.23980/

## To download kernel
https://gitlab.com/houselabs/pve-remove-rmrr/-/releases


## For 5.4.x kernel
Try this guide if you get the following error after you upgraded to 5.4.x kernel.
```
kvm: -device vfio-pci,host=0000:00:1f.2,id=hostpci0,bus=pci.0,addr=0x10: VFIO_MAP_DMA failed: Invalid argument
kvm: -device vfio-pci,host=0000:00:1f.2,id=hostpci0,bus=pci.0,addr=0x10: vfio 0000:00:1f.2: failed to setup container for group 10: memory listener initialization failed: Region pc.bios: vfio_dma_map(0x7f2553267200, 0xe0000, 0x20000, 0x7f234d620000) = -22 (Invalid argument)
```
1. Edit /etc/default/grub
2. Add option `vfio_iommu_type1.allow_unsafe_interrupts=1` to `GRUB_CMDLINE_LINUX_DEFAULT`
   For example: 
   ```
    GRUB_CMDLINE_LINUX_DEFAULT="quiet intel_iommu=on vfio_iommu_type1.allow_unsafe_interrupts=1"
   ```
3. update-grub
4. reboot

**RELATED ISSUES**:
- https://forum.proxmox.com/threads/iommu-unsafe-interrupts-enabled-still-error-message.67341/

## To get version from pve-kernel repo
`git log --format=oneline --grep="bump version to 5.4"`
