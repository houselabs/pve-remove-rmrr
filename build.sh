#!/bin/bash
KERNEL_COMMIT="3507a8ec4c93cfd074f62fdc4d07a634aff607ad"

cd /usr/src/pve-kernel
git fetch --all
git checkout $KERNEL_COMMIT
git reset --hard HEAD
git clean -d -x -ff

make clean
make update_modules
git submodule update --init --recursive

# Generate kernel patch
export `grep "KERNEL_SRC=" Makefile`
cd /usr/src/pve-kernel/submodules/$KERNEL_SRC
SEARCH_FILE="drivers/iommu/intel-iommu.c"
SEARCH_TARGET="Device is ineligible for IOMMU domain attach due to platform RMRR requirement.  Contact your platform vendor"
sed -i -e "/$SEARCH_TARGET/{n;d}" $SEARCH_FILE
sed -i "s/$SEARCH_TARGET/Device was ineligible for IOMMU domain attach due to platform RMRR requirement. Patch is in effect/g" $SEARCH_FILE
git diff > ../../patches/kernel/remove-rmrr-check.patch
echo "Generated patch:"
git diff
git checkout -- drivers/iommu/intel-iommu.c
cd -

# Patch kernel version
sed -i "s/die \"strange directory name/\#die \"strange directory name/g" /usr/src/pve-kernel/debian/scripts/find-firmware.pl
sed -i 's/EXTRAVERSION=-${KREL}-pve/EXTRAVERSION=-${KREL}-pve-removermrr/g' /usr/src/pve-kernel/Makefile

# Build
cd /usr/src/pve-kernel
echo -e "$(nproc) thread compile"
make -j$(nproc) || make -j1 || make -j1 V=s
